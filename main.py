#!/usr/bin/env python3

import multiprocessing
import os
import sys
from time import sleep

from mailworker import get_task_list
from taskprocessing import process


def daemonize(stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
    """
    This function was taken from Python Cookbook (Forking a daemon process).
    """
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError as e:
        sys.stderr.write("Fork #1 failed: {0.errno} {0.strerror}\n".format(e))
        sys.exit(1)

    #os.chdir("/")
    os.umask(0)
    os.setsid()

    #try:
    #    pid = os.fork()
    #    if pid > 0:
    #        sys.exit(0)
    #except OSError as e:
    #    sys.stderr.write("Fork #2 failed: {0.errno} {0.strerror}\n".format(e))
    #    sys.exit(1)

    for f in sys.stdout, sys.stderr:
        f.flush()
    si = open(stdin, 'r')
    so = open(stdout, 'a+')
    se = open(stderr, 'a+', 1)
    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())


if __name__ == "__main__":
    daemonize()
    while True:
        task_list = get_task_list(True)
        for task in task_list:
            p = multiprocessing.Process(target=process, args=(task,))
            p.start()
        sleep(60)
