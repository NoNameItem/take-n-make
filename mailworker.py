#!/usr/bin/env python3

import poplib
import smtplib
import email
import os.path
from email.mime.text import MIMEText

from jsonworker import *
from Logger import Logger

CONFIG = "mail_config.json"
LOGFILE = "log"
VALID_TASKS = {"TORRENT", "TEST"}
POSSIBLE_ATTACHMENTS = {"application/x-bittorrent"}


def split_subj(subj):
    command, *params = subj.split(' ')
    return command.upper(), set(params)


def find_body(message):
    for part in message.walk():
        if part.get_content_type() == 'text/plain':
            return part.get_payload().strip(" \n")


def find_attachment(message):
    for part in message.walk():
        if part.get_content_type() in POSSIBLE_ATTACHMENTS:
            at = open(part.get_filename(), 'wb')
            at.write(part.get_payload(decode=True))
            at.close()
            return os.path.abspath(part.get_filename())
    return None


def isvalid(task):
    logger = Logger(LOGFILE)
    command = task["command"]
    if command not in VALID_TASKS:
        logger.write("Bad task: {0}".format(command))
        send_mail(task["reply_to"], "Take'n'make problem", "Bad task: {0}".format(command))
        return False
    enable_whitelist, whitelist = get_param(CONFIG, ["enable_mail_whitelist", "mail_whitelist"])
    if enable_whitelist:
        if task["reply_to"] not in set(whitelist):
            logger.write("Bad mail: {0}".format(task["reply_to"]))
            send_mail(task["reply_to"], "Take'n'make problem", "You are not in white list")
            return False
    if command == "TORRENT" and ((task["attachment"] is None) and ('url' not in task["params"])):
        print(task['params'])
        logger.write("Forgotten .torrent file, {0}".format(str(task["params"])))
        send_mail(task["reply_to"], "Take'n'make problem", "Forgotten .torrent file")
        return False
    return True


def get_task_list(delete=True):
    logger = Logger(LOGFILE)
    num_messages = 0
    task_list = []
    try:
        host, port, username, password, ssl = get_param(CONFIG,
                                                        ["pop_host", "pop_port", "username", "password", "SSL"])
        if ssl:
            m = poplib.POP3_SSL(host, port)
        else:
            m = poplib.POP3(host, port)
        m.user(username)
        m.pass_(password)
        num_messages = len(m.list()[1])
        for i in range(num_messages):
            message_bytes = b"\n".join(m.retr(i + 1)[1])
            message = email.message_from_bytes(message_bytes)
            reply_to = message["Return-path"].strip("<>")
            command, params = split_subj(message["Subject"])
            body = find_body(message)
            attachment = find_attachment(message)
            task = {"command": command, "params": params, "body": body, "attachment": attachment, "reply_to": reply_to}
            if isvalid(task):
                task_list.append(task)
            if delete:
                m.dele(i + 1)
        if delete:
            m.quit()
    except ConfigFileError:
        logger.write("Can't open config file: {0}".format(CONFIG))
    except ConfigFormatError:
        logger.write("Wrong config file format: {0}".format(CONFIG))
    if num_messages != 0:
        logger.write("Found {0} new messages: {1} valid commands.\n".format(num_messages, len(task_list)))
    return task_list


def send_mail(adress, subj, message):
    logger = Logger(LOGFILE)
    try:
        host, port, username, password, ssl = get_param(CONFIG,
                                                        ["smtp_host", "smtp_port", "username", "password", "SSL"])
        if ssl:
            m = smtplib.SMTP_SSL(host, port)
        else:
            m = smtplib.SMTP(host, port)
        m.login(username, password)

        mime_message = MIMEText(message)
        mime_message["From"] = username
        mime_message["To"] = adress
        mime_message["Subject"] = subj

        m.send_message(mime_message)
        logger.write("Mail {0}: {1} sended to {2}".format(subj, message, adress))
        m.quit()
    except ConfigFileError:
        logger.write("Can't open config file: {0}".format(CONFIG))
    except ConfigFormatError:
        logger.write("Wrong config file format: {0}".format(CONFIG))


if __name__ == "__main__":
    print(get_task_list(False))
    send_mail("nonameitem@me.com", "Test", "Test")