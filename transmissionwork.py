#!/usr/bin/python3
import time
import os

import transmissionrpc

from jsonworker import *
from Logger import Logger
from mailworker import send_mail

CONFIG = "transmission_config.json"
LOGFILE = "log"


class NotEnoughSpaceError(Exception):
    pass


def get_torrent_size(torrent):
    size = 0
    for file in torrent.files().values():
        size += file['size']
    return size


def connect(host="localhost", port=9091, auth_need=False, login=None, password=None):
    if not auth_need:
        return transmissionrpc.Client(host, port)
    else:
        return transmissionrpc.Client(host, port, login, password)


def download(path, answer_mail, delete_after_download=False):
    logger = Logger(LOGFILE)
    try:
        host, port, auth_need, login, password = get_param(CONFIG,
                                                           ["host", "port", "auth_need", "login", "password"])
        client = connect(host, port, auth_need, login, password)
        session = client.get_session()
        session.update()
        free_space = session.download_dir_free_space
        torrent = client.add_torrent(path)
        torrent.update()
        print(free_space)
        if free_space < get_torrent_size(torrent):
            raise NotEnoughSpaceError
        logger.write("Torrent {0} added.".format(torrent.name))
        send_mail(answer_mail, "Take'n'Make", "Torrent {0} added.\n".format(torrent.name))
        while not torrent.status == 'seeding':
            time.sleep(60)
            torrent.update()
        logger.write("Torrent {0} finished.".format(torrent.name))
        send_mail(answer_mail, "Take'n'Make", "Torrent {0} finished.".format(torrent.name))
        if delete_after_download:
            client.remove_torrent(torrent.id)
    except NotEnoughSpaceError:
        client.remove_torrent(torrent.id)
        logger.write("Not enough free space to download {0}. Task cancelled".format(torrent.name))
        send_mail(answer_mail, "Take'n'Make problem",
                  "Not enough free space to download {0}. Task cancelled".format(torrent.name))
    except ConfigFileError:
        logger.write("Can't open config file: {0}".format(CONFIG))
        send_mail(answer_mail, "Take'n'make problem", "Can't open config file: {0}".format(CONFIG))
    except ConfigFormatError:
        logger.write("Wrong config file format: {0}".format(CONFIG))
        send_mail(answer_mail, "Take'n'make problem", "Wrong config file format: {0}".format(CONFIG))
    except transmissionrpc.TransmissionError as e:
        logger.write("Can't work with transmission. Check {0} and transmission setting. More: {1}".format(CONFIG, e))
        send_mail(answer_mail, "Take'n'make problem",
                  "Can't work with transmission. Check {0} and transmission setting. More: {1}".format(CONFIG, e))
    finally:
        if not path.startswith("http"):
            os.remove(path)
