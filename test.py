"""
Test script. It doesn't daemonize, but it checks specified email once
and process messages like daemon should do on each iteration
"""

import multiprocessing

from taskprocessing import process
from mailworker import get_task_list


if __name__ == "__main__":
    task_list = get_task_list(False)
    for task in task_list:
        p = multiprocessing.Process(target=process, args=(task,))
        p.start()
