#!/usr/bin/env python3

import time, os.path
from datetime import datetime


def create_open(filename):
    if os.path.exists(filename):
        return open(filename, 'a')
    else:
        return open(filename, 'w')


class Logger():
    __non_free = set()

    def __init__(self, file):
        self.file = file

    def write(self, msg):
        while self.file in Logger.__non_free:
            time.sleep(0.001)
        Logger.__non_free.add(self.file)
        fp = create_open(self.file)
        fp.write("{0} : {1}\n".format(datetime.now().strftime("%X %d.%m.%Y"), msg))
        fp.close()
        Logger.__non_free.remove(self.file)