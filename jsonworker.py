#!/usr/bin/env python3

import json


class ConfigFileError(Exception):
    pass


class ConfigFormatError(Exception):
    pass


def get_param(file, params_list):
    fp = None
    try:
        fp = open(file)
        conf_dict = json.load(fp)
        conf_tuple = tuple(map(lambda x: conf_dict[x], params_list))
        return conf_tuple
    except OSError:
        raise ConfigFileError()
    except KeyError:
        raise ConfigFormatError()
    finally:
        if fp:
            fp.close()