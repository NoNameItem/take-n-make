from transmissionwork import download


def process_torrent(task):
    if "url" not in task["params"]:
        if "del" not in task["params"]:
            download(task["attachment"], task["reply_to"])
        else:
            download(task["attachment"], task["reply_to"], True)
    else:
        if "del" not in task["params"]:
            download(task["body"], task["reply_to"])
        else:
            download(task["body"], task["reply_to"], True)


def process(task):
    if task["command"] == "TORRENT":
        process_torrent(task)

